# python-dm3068
Python interfaces and utilities for the Rigol DM3068 6 1/2 digit digital multimeter

* These need commoning out/refactoring into a single program

* charge-load-monitor Logs voltage, current, amp and watt hours to standard output and a file
* current-monitor Logs current to standard output and a file
* temp-monitor Logs thermocouple/RTC/thermistor temp to standard output and a file
* voltage-monitor Logs voltage to standard output and a file
